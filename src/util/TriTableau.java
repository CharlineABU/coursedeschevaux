/**
 * 
 */
package util;

/**
 * @author 59013-07-04
 *
 */
public class TriTableau {

    /**
     * Permet de v�rifier qu'un indice est dans un intervalle
     * 
     * @param intervalleMin
     * @param intervalleMax
     * @param indice
     * @return true si l'indice est compris entre intervalleMin inclu <br>
     *         et intervalleMax exclu
     */
    private static boolean isDansIntervalle(final int intervalleMin, final int intervalleMax, final int indice) {
        return indice >= intervalleMin && indice < intervalleMax;
    }

    /**
     * Permet d'inverser les cases i et j du tableau
     * 
     * @param tab le tableau
     * @param i un indice du tableau
     * @param j un indice du tableau
     */
    public static void inverserValeur(final int[] tab, final int i, final int j) {
        // pour v�rifier que les indices sont dans le tableau
        if (isDansIntervalle(0, tab.length, i) && isDansIntervalle(0, tab.length, j)) {
            final int temp = (int) tab[i];
            tab[i] = tab[j];
            tab[j] = temp;
        }
    }

    /**
     * tri du tableau par ordre decroissant
     * 
     * @param fondDeCaisse
     */
    public static void trierDecroissant(final int[] tab) {
        for (int i = 0; i < tab.length - 1; i++) {
            for (int j = i; j < tab.length; j++) {
                if (tab[i] < tab[j]) {
                    inverserValeur(tab, i, j);
                }
            }
        }
    }

    /**
     * tri du tableau par ordre croissant
     * 
     * @param fondDeCaisse
     */
    public static void trierCroissant(final int[] tab) {
        for (int i = 0; i < tab.length - 1; i++) {
            for (int j = i; j < tab.length; j++) {
                if (tab[j] < tab[i]) {
                    inverserValeur(tab, i, j);
                }
            }
        }
    }

    /**
     * Permet de recherche le minimum � partir d'un indice du tableau
     * 
     * @param tab
     * @param indiceDebut
     * @return l'indice du minimum
     */
    private static int rechercherMinimum(final int[] tab, final int indiceDebut) {
        // on initialise le minimum avec la "indiceDebut"�me case du tableau
        int indiceTemp = indiceDebut;
        double minimumTemp = tab[indiceTemp];

        // pour chaque case du tableau
        for (int i = indiceDebut + 1; i < tab.length; i++) {
            final double nombreCourant = tab[i];
            if (nombreCourant < minimumTemp) {
                // on stocke le nouveau minimum
                minimumTemp = nombreCourant;
                // on stocke l'indice de ce minimum
                indiceTemp = i;
            }
        }
        return indiceTemp;
    }

    /**
     * Permet de trier un tableau par ordre croissant utilisation de la recharche du minimum
     * 
     * @param tab
     */
    public static void triTableau(final int[] tab) {
        for (int i = 0; i < tab.length; i++) {
            // on recherche le minimum � partir d'un indice du tableau
            final int indiceMin = rechercherMinimum(tab, i);
            // on r�utilise
            inverserValeur(tab, i, indiceMin);
        }
    }

    /**
     * @param args
     */
    public static void main(final String[] args) {
        int[] tableau = { 1, 6, 7, 2, 3, 7, 56, 4, 45, 65, 4, 12 };

        trierCroissant(tableau);
        //affiche le tableau tri� par ordre croissant
        System.out.println("tri croissant");
        for (int i = 0; i < tableau.length; i++) {
            System.out.print(tableau[i] + " , ");
        }
        System.out.println();

        trierDecroissant(tableau);
        //affiche le tableau tri� par ordre d�croissant
        System.out.println("tri decroissant");
        for (int i = 0; i < tableau.length; i++) {
            System.out.print(tableau[i] + " , ");
        }
        System.out.println();
        
        triTableau(tableau);
        //affiche le tableau tri� par ordre croissant
        System.out.println("tri croissant par recherche de minimum");
        for (int i = 0; i < tableau.length; i++) {
            System.out.print(tableau[i] + " , ");
        }
        System.out.println();

    }

}
