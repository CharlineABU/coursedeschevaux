/**
 * 
 */
package model;

/**
 * Objet Cheval :
 * 
 * @author Charline
 *
 */
public class Cheval {

    private int        id;
    private static int compteurId;
    private String     nom;
    private int     x, y;
    private String     photoCheval;

    /**
     * Constructeur par defaut
     */
    public Cheval() {
        super();
        Cheval.compteurId++;
    }

    /**
     * Constructeur avec field
     * 
     * @param nom
     * @param x
     * @param y
     * @param photoCheval
     */
    public Cheval(final String nom, final int x, final int y, final String photoCheval) {
        compteurId++;
        this.id = compteurId;
        this.nom = nom;
        this.x = x;
        this.y = y;
        this.photoCheval = photoCheval;
    }

    @Override
    public String toString() {
        return "Cheval : \n     Num Identification : " + id + " Nom cheval : " + nom + " -  Position : (" + x + "," + y + ") - Url Photo : " + photoCheval + ";";
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(final int id) {
        this.id = id;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(final String nom) {
        this.nom = nom;
    }

    /**
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public void setX(final int x) {
        this.x = x;
    }

    /**
     * @return the y
     */
    public int getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public void setY(final int y) {
        this.y = y;
    }

    /**
     * @return the photoCheval
     */
    public String getPhotoCheval() {
        return photoCheval;
    }

    /**
     * @param photoCheval the photoCheval to set
     */
    public void setPhotoCheval(final String photoCheval) {
        this.photoCheval = photoCheval;
    }

}
