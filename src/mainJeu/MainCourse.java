/**
 * 
 */
package mainJeu;

import java.awt.Color;

import javax.swing.JFrame;

import vue.Scene;

/**
 * @author Charline
 *
 */
public class MainCourse {

    public static Scene scene;

    /**
     * @param args
     */
    public static void main(final String[] args) {

        //construction de la frame
        maJFrame();
        try {
            Thread.sleep(5000);
            //Thread.currentThread().interrupt();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    /**
     * 
     */
    public static JFrame maJFrame() {
        JFrame fenetre = new JFrame("Course de cheval");
        fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fenetre.setSize(1200, 700);
        fenetre.setLocationRelativeTo(null);
        fenetre.setResizable(false);
        fenetre.setAlwaysOnTop(true);
        fenetre.setBackground(Color.yellow);

        scene = new Scene();
        fenetre.setContentPane(scene);
        fenetre.setVisible(true);
        return fenetre;
    }

}
