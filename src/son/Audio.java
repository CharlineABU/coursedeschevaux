/**
 * 
 */
package son;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

/**
 * @author Charline
 *
 */
public class Audio {
    
    private Clip clip;
    
    /**
     * Constructeur par defaut
     */
    public Audio() {
        super();
    }

    /**
     * 
     */
    public Audio(final String son) {
        try {
            AudioInputStream audio = AudioSystem.getAudioInputStream(getClass().getResource(son));
            clip = AudioSystem.getClip();
            clip.open(audio);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    public void play() {
        clip.start();
    }
    
    public void stop() {
        clip.stop();
    }
    
    public static void playSound(final String son) {
        Audio musique = new Audio(son);
        musique.play();
    }
    

    public Clip getClip() {
        return clip;
    }

    public void setClip(final Clip clip) {
        this.clip = clip;
    }
    
    

}
