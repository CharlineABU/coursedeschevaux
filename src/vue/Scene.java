/**
 * 
 */
package vue;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import mainJeu.MainCourse;
import model.Cheval;
import son.Audio;

/**
 * @author Charline
 *
 */
@SuppressWarnings("serial")
public class Scene extends JPanel implements Runnable {

    private final static int PAUSE          = 200;
    private final static int LONGUEUR_PISTE = 1000;

    private Thread           chronoCourse;

    private ImageIcon        icoFond;
    private Image            imgFond;

    private Cheval           cheval1;
    private ImageIcon        icoCheval1;
    private Image            imgCheval1;

    private Cheval           cheval2;
    private ImageIcon        icoCheval2;
    private Image            imgCheval2;

    private Cheval           cheval3;
    private ImageIcon        icoCheval3;
    private Image            imgCheval3;

    private Cheval           cheval4;
    private ImageIcon        icoCheval4;
    private Image            imgCheval4;

    private Cheval           cheval5;
    private ImageIcon        icoCheval5;
    private Image            imgCheval5;

    private int              xFond;

    private static boolean   condition1;
    private static boolean   condition2;
    private static boolean   condition3;
    private static boolean   condition4;
    private static boolean   condition5;

    /**
     * Constructeur par defaut
     */
    public Scene() {
        super();
        this.cheval1 = new Cheval("toto", 2, 90, "/images/cheval1.png");
        this.cheval2 = new Cheval("titi", 2, 202, "/images/cheval2.png");
        this.cheval3 = new Cheval("tata", 2, 315, "/images/cheval3.png");
        this.cheval4 = new Cheval("tete", 2, 430, "/images/cheval4.png");
        this.cheval5 = new Cheval("tutu", 2, 540, "/images/cheval5.png");
        this.xFond = 0;

        this.icoFond = new ImageIcon(getClass().getResource("/images/piste.png"));
        this.imgFond = this.icoFond.getImage();
        this.icoCheval1 = new ImageIcon(getClass().getResource(this.cheval1.getPhotoCheval()));
        this.imgCheval1 = this.icoCheval1.getImage();
        this.icoCheval2 = new ImageIcon(getClass().getResource(this.cheval2.getPhotoCheval()));
        this.imgCheval2 = this.icoCheval2.getImage();
        this.icoCheval3 = new ImageIcon(getClass().getResource(this.cheval3.getPhotoCheval()));
        this.imgCheval3 = this.icoCheval3.getImage();
        this.icoCheval4 = new ImageIcon(getClass().getResource(this.cheval4.getPhotoCheval()));
        this.imgCheval4 = this.icoCheval4.getImage();
        this.icoCheval5 = new ImageIcon(getClass().getResource(this.cheval5.getPhotoCheval()));
        this.imgCheval5 = this.icoCheval5.getImage();

    }

    /**
     * Permet de calculer une distance de déplacement aléatoire
     * 
     * @param x la position de depart
     * @return deplacement à effectuer
     */
    public int calculDistance(final int x) {
        final Random r = new Random();
        final int low = 10;
        final int high = 30;
        final int newPosition = x + r.nextInt(high - low) + low;

        return newPosition;
    }

    /**
     * lance la course des chevaux
     */
    public void courir() {

        int posCheval1 = calculDistance(MainCourse.scene.getCheval1().getX());
        int posCheval2 = calculDistance(MainCourse.scene.getCheval2().getX());
        int posCheval3 = calculDistance(MainCourse.scene.getCheval3().getX());
        int posCheval4 = calculDistance(MainCourse.scene.getCheval4().getX());
        int posCheval5 = calculDistance(MainCourse.scene.getCheval5().getX());

        MainCourse.scene.getCheval1().setX(posCheval1);
        MainCourse.scene.getCheval2().setX(posCheval2);
        MainCourse.scene.getCheval3().setX(posCheval3);
        MainCourse.scene.getCheval4().setX(posCheval4);
        MainCourse.scene.getCheval5().setX(posCheval5);

    }

    /**
     * Permet d'afficher le vainqueur de la course
     * 
     * @return le tableau par ordre au premier arrivé
     */
    public String[] vainqueur() {
        int posCheval1 = MainCourse.scene.getCheval1().getX();
        int posCheval2 = MainCourse.scene.getCheval2().getX();
        int posCheval3 = MainCourse.scene.getCheval3().getX();
        int posCheval4 = MainCourse.scene.getCheval4().getX();
        int posCheval5 = MainCourse.scene.getCheval5().getX();

        final int tabPositions[] = { posCheval1, posCheval2, posCheval3, posCheval4, posCheval5 };
        util.TriTableau.trierDecroissant(tabPositions);
        final String resultat[] = new String[tabPositions.length];
        for (int i = 0; i < tabPositions.length; i++) {
            int pos = 1;

            if (tabPositions[i] == posCheval1) {
                pos = 1 + i;
                resultat[i] = "TOTO est N°" + pos;
            }
            if (tabPositions[i] == posCheval2) {
                pos = 1 + i;
                resultat[i] = "TITI est N°" + pos;
            }
            if (tabPositions[i] == posCheval3) {
                pos = 1 + i;
                resultat[i] = "TATA est N°" + pos;
            }
            if (tabPositions[i] == posCheval4) {
                pos = 1 + i;
                resultat[i] = "TETE est N°" + pos;
            }
            if (tabPositions[i] == posCheval5) {
                pos = 1 + i;
                resultat[i] = "TUTU est N°" + pos;
            }
        }

        return resultat;

    }

    @Override
    public void run() {
        Audio.playSound("/audio/rock.wav");
        condition1 = MainCourse.scene.getCheval1().getX() < LONGUEUR_PISTE;
        condition2 = MainCourse.scene.getCheval2().getX() < LONGUEUR_PISTE;
        condition3 = MainCourse.scene.getCheval3().getX() < LONGUEUR_PISTE;
        condition4 = MainCourse.scene.getCheval4().getX() < LONGUEUR_PISTE;
        condition5 = MainCourse.scene.getCheval5().getX() < LONGUEUR_PISTE;

        MainCourse.scene.repaint();
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        while (condition1 && condition2 && condition3 && condition4 && condition5) {
            courir();
            MainCourse.scene.repaint();
            System.out.println("Cheval1 (" + MainCourse.scene.getCheval1().getX() + "," + MainCourse.scene.getCheval1().getY() + ")");
            System.out.println("Cheval2 (" + MainCourse.scene.getCheval2().getX() + "," + MainCourse.scene.getCheval2().getY() + ")");
            System.out.println("Cheval3 (" + MainCourse.scene.getCheval3().getX() + "," + MainCourse.scene.getCheval3().getY() + ")");
            System.out.println("Cheval4 (" + MainCourse.scene.getCheval4().getX() + "," + MainCourse.scene.getCheval4().getY() + ")");
            System.out.println("Cheval5 (" + MainCourse.scene.getCheval5().getX() + "," + MainCourse.scene.getCheval5().getY() + ")");
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            condition1 = MainCourse.scene.getCheval1().getX() < LONGUEUR_PISTE;
            condition2 = MainCourse.scene.getCheval2().getX() < LONGUEUR_PISTE;
            condition3 = MainCourse.scene.getCheval3().getX() < LONGUEUR_PISTE;
            condition4 = MainCourse.scene.getCheval4().getX() < LONGUEUR_PISTE;
            condition5 = MainCourse.scene.getCheval5().getX() < LONGUEUR_PISTE;
            System.out.println("cond1 = " + condition1 + "\ncond2 = " + condition2 + "\ncond3 = " + condition3 + "\ncond4 = " + condition4 + "\ncond5 = " + condition5);
            System.out.println("-------------------");

            if (!condition1 || !condition2 || !condition3 || !condition4 || !condition5) {
                MainCourse.scene.repaint();

            }
        }

    }

    @Override
    protected void paintComponent(final Graphics g) {
        super.paintComponent(g);
        final Graphics g2 = (Graphics2D) g;
        final Font policeFont = new Font("Comic Sans MS", Font.BOLD, 40);

        g2.setFont(policeFont);

        g2.drawImage(this.imgFond, this.xFond, 0, null);
        g2.drawString("** COURSE DE LILLE ** ", 400, 70);
        g2.drawImage(imgCheval1, this.cheval1.getX(), this.cheval1.getY(), null);
        g2.drawImage(imgCheval2, this.cheval2.getX(), this.cheval2.getY(), null);
        g2.drawImage(imgCheval3, this.cheval3.getX(), this.cheval3.getY(), null);
        g2.drawImage(imgCheval4, this.cheval4.getX(), this.cheval4.getY(), null);
        g2.drawImage(imgCheval5, this.cheval5.getX(), this.cheval5.getY(), null);

        if (!condition1 || !condition2 || !condition3 || !condition4 || !condition5) {
            final Font policeFont2 = new Font("Comic Sans MS", Font.CENTER_BASELINE, 30);
            g2.setColor(Color.white);
            g2.setFont(policeFont2);

            g2.drawString("FIN DE COURSE  ! ", 200, 170);
            g2.drawString("----*----*****----*----", 250, 250);
            final String resultat[] = vainqueur();
            int y = 300;
            for (final String string : resultat) {
                g2.drawString(string, 300, y);
                y = y + 50;
            }
            g2.drawString("Fermer l'ecran pour sortir ! ", 200, 600);

        }

    }

    @Override
    public void addNotify() {
        super.addNotify();
        chronoCourse = new Thread(this);
        chronoCourse.start();
    }

    public Thread getChronoCourse() {
        return chronoCourse;
    }

    public void setChronoCourse(final Thread chronoCourse) {
        this.chronoCourse = chronoCourse;
    }

    public ImageIcon getIcoFond() {
        return icoFond;
    }

    public void setIcoFond(final ImageIcon icoFond) {
        this.icoFond = icoFond;
    }

    public Image getImgFond() {
        return imgFond;
    }

    public void setImgFond(final Image imgFond) {
        this.imgFond = imgFond;
    }

    public Cheval getCheval1() {
        return cheval1;
    }

    public void setCheval1(final Cheval cheval1) {
        this.cheval1 = cheval1;
    }

    public ImageIcon getIcoCheval1() {
        return icoCheval1;
    }

    public void setIcoCheval1(final ImageIcon icoCheval1) {
        this.icoCheval1 = icoCheval1;
    }

    public Image getImgCheval1() {
        return imgCheval1;
    }

    public void setImgCheval1(final Image imgCheval1) {
        this.imgCheval1 = imgCheval1;
    }

    public Cheval getCheval2() {
        return cheval2;
    }

    public void setCheval2(final Cheval cheval2) {
        this.cheval2 = cheval2;
    }

    public ImageIcon getIcoCheval2() {
        return icoCheval2;
    }

    public void setIcoCheval2(final ImageIcon icoCheval2) {
        this.icoCheval2 = icoCheval2;
    }

    public Image getImgCheval2() {
        return imgCheval2;
    }

    public void setImgCheval2(final Image imgCheval2) {
        this.imgCheval2 = imgCheval2;
    }

    public Cheval getCheval3() {
        return cheval3;
    }

    public void setCheval3(final Cheval cheval3) {
        this.cheval3 = cheval3;
    }

    public ImageIcon getIcoCheval3() {
        return icoCheval3;
    }

    public void setIcoCheval3(final ImageIcon icoCheval3) {
        this.icoCheval3 = icoCheval3;
    }

    public Image getImgCheval3() {
        return imgCheval3;
    }

    public void setImgCheval3(final Image imgCheval3) {
        this.imgCheval3 = imgCheval3;
    }

    public Cheval getCheval4() {
        return cheval4;
    }

    public void setCheval4(final Cheval cheval4) {
        this.cheval4 = cheval4;
    }

    public ImageIcon getIcoCheval4() {
        return icoCheval4;
    }

    public void setIcoCheval4(final ImageIcon icoCheval4) {
        this.icoCheval4 = icoCheval4;
    }

    public Image getImgCheval4() {
        return imgCheval4;
    }

    public void setImgCheval4(final Image imgCheval4) {
        this.imgCheval4 = imgCheval4;
    }

    public Cheval getCheval5() {
        return cheval5;
    }

    public void setCheval5(final Cheval cheval5) {
        this.cheval5 = cheval5;
    }

    public ImageIcon getIcoCheval5() {
        return icoCheval5;
    }

    public void setIcoCheval5(final ImageIcon icoCheval5) {
        this.icoCheval5 = icoCheval5;
    }

    public Image getImgCheval5() {
        return imgCheval5;
    }

    public void setImgCheval5(final Image imgCheval5) {
        this.imgCheval5 = imgCheval5;
    }

    public static boolean isCondition1() {
        return condition1;
    }

    public static void setCondition1(final boolean condition1) {
        Scene.condition1 = condition1;
    }

    public static boolean isCondition2() {
        return condition2;
    }

    public static void setCondition2(final boolean condition2) {
        Scene.condition2 = condition2;
    }

    public static boolean isCondition3() {
        return condition3;
    }

    public static void setCondition3(final boolean condition3) {
        Scene.condition3 = condition3;
    }

    public static boolean isCondition4() {
        return condition4;
    }

    public static void setCondition4(final boolean condition4) {
        Scene.condition4 = condition4;
    }

    public static boolean isCondition5() {
        return condition5;
    }

    public static void setCondition5(final boolean condition5) {
        Scene.condition5 = condition5;
    }

    public int getxFond() {
        return xFond;
    }

    public void setxFond(final int xFond) {
        this.xFond = xFond;
    }

    public static int getPause() {
        return PAUSE;
    }

    public static int getLongueurPiste() {
        return LONGUEUR_PISTE;
    }

}
